import 'package:flutter/widgets.dart';
import 'package:primaku_test/data/models/model.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:primaku_test/data/repositories/services/movies_services.dart';
import 'package:flutter_network_connectivity/flutter_network_connectivity.dart';

// enum PopularState { initial, loading, loaded, error, noInternet }

class PopularMoviesProvider extends ChangeNotifier {
  state _popularState = state.initial;
  GeneralMovieModel? _dataSucces;

  state get popularState => _popularState;
  GeneralMovieModel? get dataSucces => _dataSucces;

  Future getData() async {
    FlutterNetworkConnectivity flutterNetworkConnectivity =
        FlutterNetworkConnectivity(
      isContinousLookUp:
          true, // optional, false if you cont want continous lookup
      lookUpDuration: const Duration(
          seconds: 5), // optional, to override default lookup duration
      lookUpUrl: 'example.com', // optional, to override default lookup url
    );

    await flutterNetworkConnectivity
        .isInternetConnectionAvailable()
        .then((value) async {
      // if internet Available
      if (value == true) {
        try {
          final data = await MoviesSevices().getPopularMoviesData();
          if (data.toString() == "Instance of 'GeneralMovieModel'") {
            _dataSucces = data;
            _popularState = state.loaded;
          } else {
            _popularState = state.error;
          }
        } catch (e) {
          _popularState = state.error;
        }
      } else {
        _popularState = state.noInternet;
      }
    });
    notifyListeners();
  }
}
