import 'package:flutter/material.dart';

enum state { initial, loading, loaded, error, noInternet }

class Const {
  final Color primaryColor = const Color(0xff15141f);

  final String apiKey = '682de3f4dc084992ebf827eea7c200a2';
}

class Urls {
  final String baseUrl = 'https://api.themoviedb.org/3/';
  final String rootImageUurl = 'https://image.tmdb.org/t/p/w500';
}
