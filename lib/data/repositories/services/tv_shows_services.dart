import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:primaku_test/data/models/model.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';

class TVShowsSevices {
  GeneralTvModel? _generalTvModel;
  GeneralFailModel? _generalFailModel;

  // get data
  Future<dynamic> getTVShowsData(String type) async {
    String url = Urls().baseUrl +
        'tv/$type?api_key=${Const().apiKey}&language=en-US&page=1';

    http.Response response = await http
        .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
    int statusCode = response.statusCode;
    final body = json.decode(response.body);
    if (statusCode == 200) {
      _generalTvModel = GeneralTvModel.fromJson(body);
      return _generalTvModel;
    } else {
      try {
        _generalFailModel = GeneralFailModel.fromJson(body);
        return _generalFailModel;
      } catch (e) {
        return _generalFailModel;
      }
    }
  }
}
