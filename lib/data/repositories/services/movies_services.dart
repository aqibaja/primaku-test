import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:primaku_test/data/models/model.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';

class MoviesSevices {
  GeneralMovieModel? _popularMoviesModel;
  GeneralFailModel? _generalFailModel;
  // get data
  Future<dynamic> getPopularMoviesData() async {
    String url = Urls().baseUrl +
        'movie/popular?api_key=${Const().apiKey}&language=en-US&page=1';

    http.Response response = await http
        .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
    int statusCode = response.statusCode;
    final body = json.decode(response.body);
    if (statusCode == 200) {
      _popularMoviesModel = GeneralMovieModel.fromJson(body);
      return _popularMoviesModel;
    } else {
      try {
        _generalFailModel = GeneralFailModel.fromJson(body);
        return _generalFailModel;
      } catch (e) {
        return _generalFailModel;
      }
    }
  }

  // get data
  Future<dynamic> getNowPlayingMoviesData(String type) async {
    String url = Urls().baseUrl +
        'movie/$type?api_key=${Const().apiKey}&language=en-US&page=1';

    http.Response response = await http
        .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
    int statusCode = response.statusCode;
    final body = json.decode(response.body);
    if (statusCode == 200) {
      _popularMoviesModel = GeneralMovieModel.fromJson(body);
      return _popularMoviesModel;
    } else {
      try {
        _generalFailModel = GeneralFailModel.fromJson(body);
        return _generalFailModel;
      } catch (e) {
        return _generalFailModel;
      }
    }
  }
}
