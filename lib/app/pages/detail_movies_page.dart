import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:primaku_test/data/models/model.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailMoviesPage extends StatefulWidget {
  final Result? dataMovie;
  const DetailMoviesPage({Key? key, this.dataMovie}) : super(key: key);

  @override
  State<DetailMoviesPage> createState() => _DetailMoviesPageState();
}

class _DetailMoviesPageState extends State<DetailMoviesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const().primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: Const().primaryColor,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 350.h,
                  width: 375.w,
                  child: Image.network(
                    Urls().rootImageUurl + (widget.dataMovie?.posterPath ?? ""),
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 24.h,
                      ),
                      Text(
                        widget.dataMovie?.title ?? "",
                        style: TextStyle(color: Colors.white, fontSize: 24.sp),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Row(
                        children: [
                          Icon(Icons.star_half_rounded,
                              size: 25.r, color: const Color(0xffBBBBBB)),
                          SizedBox(
                            width: 5.w,
                          ),
                          Text(
                            (widget.dataMovie?.voteAverage ?? "").toString(),
                            style: TextStyle(
                                color: const Color(0xffBBBBBB),
                                fontSize: 15.sp),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      const Divider(
                        color: Color(0xffBBBBBB),
                        thickness: 0.5,
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Release date",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp),
                      ),
                      SizedBox(
                        height: 12.h,
                      ),
                      Text(
                        widget.dataMovie?.releaseDate ?? "",
                        style: TextStyle(
                            color: const Color(0xffBCBCBC), fontSize: 12.sp),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      const Divider(
                        color: Color(0xffBBBBBB),
                        thickness: 0.5,
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      Text(
                        "Synopsis",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp),
                      ),
                      SizedBox(
                        height: 12.h,
                      ),
                      Text(
                        widget.dataMovie?.overview ?? "",
                        maxLines: 3,
                        style: TextStyle(
                            color: const Color(0xffBCBCBC), fontSize: 12.sp),
                      ),
                      Text(
                        "Read more..",
                        style: TextStyle(color: Colors.white, fontSize: 12.sp),
                      ),
                      SizedBox(
                        height: 15.h,
                      ),
                      const Divider(
                        color: Color(0xffBBBBBB),
                        thickness: 0.5,
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
