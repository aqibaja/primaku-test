import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flappy_search_bar_ns/search_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:primaku_test/app/pages/detail_tv_page.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:primaku_test/data/models/model.dart';

class TvSearch extends StatefulWidget {
  const TvSearch({Key? key}) : super(key: key);

  @override
  _TvSearchState createState() => _TvSearchState();
}

class _TvSearchState extends State<TvSearch> {
  final SearchBarController<GeneralTvResult> _searchBarController =
      SearchBarController();
  bool isReplay = false;

  Future<List<GeneralTvResult>> _getMoviesData(String? text) async {
    String url = Urls().baseUrl +
        'search/tv?api_key=${Const().apiKey}&language=en-US&query=$text&page=1&include_adult=false';

    http.Response response = await http
        .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
    int statusCode = response.statusCode;
    final body = json.decode(response.body);

    GeneralTvModel? _tvModel;

    if (statusCode == 200) {
      _tvModel = GeneralTvModel.fromJson(body);
      return _tvModel.results!;
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const().primaryColor,
      body: SafeArea(
        child: SearchBar<GeneralTvResult>(
          searchBarPadding: const EdgeInsets.symmetric(horizontal: 10),
          headerPadding: const EdgeInsets.symmetric(horizontal: 10),
          listPadding: const EdgeInsets.symmetric(horizontal: 10),
          onSearch: _getMoviesData,
          searchBarController: _searchBarController,
          onError: (error) => Text('ERROR: ${error.toString()}'),
          placeHolder: const Center(
              child: Text(
            "Search",
            style: TextStyle(color: Colors.white),
          )),
          cancellationWidget: const Text(
            "Cancel",
            style: TextStyle(color: Colors.white),
          ),
          emptyWidget: const Center(
              child: Text(
            "empty data",
            style: TextStyle(color: Colors.white),
          )),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          crossAxisCount: 2,
          searchBarStyle: const SearchBarStyle(
              backgroundColor: Color(0xff211F30),
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          icon: const Icon(
            Icons.search,
            color: Colors.white,
          ),
          textStyle: const TextStyle(color: Colors.white),
          onItemFound: (GeneralTvResult? post, int index) {
            return InkWell(
              onTap: () => Navigator.of(context, rootNavigator: true)
                  .push(MaterialPageRoute(
                      builder: (_) => DetailTvPage(
                            dataTvShow: post,
                          ))),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(Urls().rootImageUurl +
                    (post?.posterPath ?? "/nk7e8RnX9ikpZnycoLRx48ieQ8A.jpg")),
              ),
            );
          },
        ),
      ),
    );
  }
}
