import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:primaku_test/app/pages/tv_show.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'app/pages/home.dart';
import 'app/pages/movie.dart';
import 'app/widget/dissmiss_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PersistentTabController? _controller;

  @override
  void initState() {
    _controller = PersistentTabController(initialIndex: 0);

    super.initState();
  }

  // list screen
  List<Widget> _buildScreens() {
    return [
      const HomeMoviePage(),
      const MoviePage(),
      const TvShowPage(),
    ];
  }

  // nav bar item
  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      itemNav('assets/nav1.png', "assets/nav1-off.png", "Home"),
      itemNav('assets/nav2.png', "assets/nav2-off.png", "Movies"),
      itemNav('assets/nav2.png', "assets/nav2-off.png", "Tv Shows"),
    ];
  }

//  widget item navbar
  PersistentBottomNavBarItem itemNav(
      String imageActive, String imageNonActive, String title) {
    return PersistentBottomNavBarItem(
      icon: Column(
        children: [
          Image.asset(imageActive, width: 24.w, height: 24.h, fit: BoxFit.fill),
          SizedBox(
            height: 4.h,
          ),
          Text(
            title,
            style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 10.sp,
                color: Colors.white,
                fontWeight: FontWeight.w600),
          )
        ],
      ),
      inactiveIcon: Column(
        children: [
          Image.asset(
            imageNonActive,
            width: 24.w,
            height: 24.h,
            fit: BoxFit.fill,
          ),
          SizedBox(
            height: 4.h,
          ),
          Text(
            title,
            style: TextStyle(
                fontFamily: 'Inter',
                fontSize: 10.sp,
                color: const Color(0xff828282).withOpacity(0.6),
                fontWeight: FontWeight.w400),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const().primaryColor,
      body: SafeArea(
        child: DismissWidget(
          child: PersistentTabView(
            context,
            controller: _controller,
            screens: _buildScreens(),
            items: _navBarsItems(),
            confineInSafeArea: true,
            backgroundColor: Const().primaryColor,
            handleAndroidBackButtonPress: true, // Default is true.
            resizeToAvoidBottomInset:
                true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
            stateManagement: true, // Default is true.
            hideNavigationBarWhenKeyboardShows:
                true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
            decoration: const NavBarDecoration(
              colorBehindNavBar: Colors.white,
            ),
            popAllScreensOnTapOfSelectedTab: true,
            popActionScreens: PopActionScreensType.all,
            /* itemAnimationProperties: const ItemAnimationProperties(
              // Navigation Bar's items animation properties.
              duration: Duration(milliseconds: 200),
              curve: Curves.ease,
            ),
            screenTransitionAnimation: const ScreenTransitionAnimation(
              // Screen transition animation on change of selected tab.
              animateTabTransition: true,
              curve: Curves.ease,
              duration: Duration(milliseconds: 200),
            ), */
            navBarHeight: 74.h,
            navBarStyle: NavBarStyle.simple,

            padding: NavBarPadding.only(
                top: 18.h), // Choose the nav bar style with this property.
          ),
        ),
      ),
    );
  }
}
