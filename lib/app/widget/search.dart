import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:primaku_test/app/widget/text_form_search.dart';

//method input form with search
Widget inputFormSearch(
  TextEditingController textEditingController, {
  bool? phone,
  List<String>? suggest,
  Widget? prefixIcon,
  bool? focusBorder,
  bool? validasi,
  bool? obscureText,
  int? maxline,
  /* ListCustomersModel? dataCustomer,
    ValueChanged<ListCustomersData>? onChanged */
}) {
  return SearchField(
    suggestions: suggest ?? [],
    number: phone,
    searchStyle: const TextStyle(color: Color(0xffBBBBBB)),
    controller: textEditingController,
    enable: false,
    searchInputDecoration: InputDecoration(
      isDense: true,
      contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
      fillColor: const Color(0xff211F30),
      prefixIcon: const Icon(
        Icons.search,
        color: Colors.white,
      ),
      filled: true,
      hintText: "Sherlock Holmes",
      hintStyle: const TextStyle(color: Color(0xffBBBBBB)),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(color: Colors.deepOrange),
        borderRadius: BorderRadius.circular(20.0),
      ),
    ),
    maxSuggestionsInViewPort: 6,
    itemHeight: 50.h,
    onTap: (x) {
      /* if (dataCustomer != null) {
        for (var i = 0; i < dataCustomer.data!.length; i++) {
          if (namesList[1] == dataCustomer.data![i].id)
            onChanged!(dataCustomer.data![i]);
        }
      } */
    },
  );
}
