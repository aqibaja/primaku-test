import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:primaku_test/data/provider/movies/movies.dart';
import 'package:primaku_test/data/provider/popular_movie.dart';
import 'package:primaku_test/data/provider/tvShows/tv_shows.dart';
import 'package:primaku_test/homapage.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: () => MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context) => PopularMoviesProvider()),
          ChangeNotifierProvider(create: (context) => MoviesProvider()),
          ChangeNotifierProvider(create: (context) => TVShowsProvider())
        ],
        child: MaterialApp(
          title: 'Flutter Primaku Test',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const HomePage(),
        ),
      ),
    );
  }
}
