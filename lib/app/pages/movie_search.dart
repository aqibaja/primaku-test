import 'package:flappy_search_bar_ns/flappy_search_bar_ns.dart';
import 'package:flappy_search_bar_ns/search_bar_style.dart';
import 'package:flutter/material.dart';
import 'package:primaku_test/app/pages/detail_movies_page.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:primaku_test/data/models/model.dart';

class MovieSearch extends StatefulWidget {
  const MovieSearch({Key? key}) : super(key: key);

  @override
  _MovieSearchState createState() => _MovieSearchState();
}

class _MovieSearchState extends State<MovieSearch> {
  final SearchBarController<Result> _searchBarController =
      SearchBarController();
  bool isReplay = false;

  /* Future<List<Post>> _getALlPosts(String? text) async {
    await Future.delayed(const Duration(seconds: 1));
    if (isReplay) return [Post("Replaying !", "Replaying body")];

    List<Post> posts = [];

    var random = new Random();
    for (int i = 0; i < 10; i++) {
      posts.add(
        Post("$text $i", "body random number : ${random.nextInt(100)}"),
      );
    }

    return posts;
  } */

  Future<List<Result>> _getMoviesData(String? text) async {
    String url = Urls().baseUrl +
        'search/movie?api_key=${Const().apiKey}&language=en-US&query=$text&page=1&include_adult=false';

    http.Response response = await http
        .get(Uri.parse(url), headers: {"Content-Type": "application/json"});
    int statusCode = response.statusCode;
    final body = json.decode(response.body);

    GeneralMovieModel? _moviesModel;

    if (statusCode == 200) {
      _moviesModel = GeneralMovieModel.fromJson(body);
      return _moviesModel.results!;
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Const().primaryColor,
      body: SafeArea(
        child: SearchBar<Result>(
          searchBarPadding: const EdgeInsets.symmetric(horizontal: 10),
          headerPadding: const EdgeInsets.symmetric(horizontal: 10),
          listPadding: const EdgeInsets.symmetric(horizontal: 10),
          onSearch: _getMoviesData,
          searchBarController: _searchBarController,
          onError: (error) => Text('ERROR: ${error.toString()}'),
          placeHolder: const Center(
              child: Text(
            "Search",
            style: TextStyle(color: Colors.white),
          )),
          cancellationWidget: const Text(
            "Cancel",
            style: TextStyle(color: Colors.white),
          ),
          emptyWidget: const Center(
              child: Text(
            "empty data",
            style: TextStyle(color: Colors.white),
          )),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          crossAxisCount: 2,
          searchBarStyle: const SearchBarStyle(
              backgroundColor: Color(0xff211F30),
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          icon: const Icon(
            Icons.search,
            color: Colors.white,
          ),
          textStyle: const TextStyle(color: Colors.white),
          onItemFound: (Result? post, int index) {
            return InkWell(
              onTap: () => Navigator.of(context, rootNavigator: true)
                  .push(MaterialPageRoute(
                      builder: (_) => DetailMoviesPage(
                            dataMovie: post,
                          ))),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(Urls().rootImageUurl +
                    (post?.posterPath ?? "/nk7e8RnX9ikpZnycoLRx48ieQ8A.jpg")),
              ),
            );
          },
        ),
      ),
    );
  }
}
