import 'package:flutter/material.dart';
import 'package:primaku_test/app/pages/detail_movies_page.dart';
import 'package:primaku_test/app/pages/movie_search.dart';
import 'package:primaku_test/app/widget/search.dart';
import 'package:primaku_test/data/provider/movies/movies.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class MoviePage extends StatefulWidget {
  const MoviePage({Key? key}) : super(key: key);

  @override
  State<MoviePage> createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  Color activeColor = const Color(0xffFF8F71);
  Color offColor = Colors.white;

  final _testList = [
    'Test Item 1',
    'Test Item 2',
    'Test Item 3',
    'Test Item 4',
  ];

  TextEditingController myController = TextEditingController();

  int selected = 1;
  bool nowPlaying = true;
  bool upcoming = false;
  bool topRated = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Const().primaryColor,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15.h,
            ),
            Text(
              "Find Movies, Tv series,\nand more..",
              style: TextStyle(color: Colors.white, fontSize: 24.sp),
            ),
            SizedBox(
              height: 20.h,
            ),
            InkWell(
                onTap: () => Navigator.of(context, rootNavigator: true).push(
                    MaterialPageRoute(builder: (_) => const MovieSearch())),
                child: inputFormSearch(myController, suggest: _testList)),
            /* SizedBox(
              child: TextFieldSearch(
                initialList: _testList,
                label: 'Movies',
                controller: myController,
                textStyle: const TextStyle(color: Color(0xffBBBBBB)),
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding:
                      const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  fillColor: const Color(0xff211F30),
                  prefixIcon: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  filled: true,
                  hintText: "Sherlock Holmes",
                  hintStyle: const TextStyle(color: Color(0xffBBBBBB)),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.deepOrange),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ), */
            SizedBox(
              height: 24.h,
            ),
            SizedBox(
              height: 55.h,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  InkWell(
                      onTap: () {
                        setState(() {
                          selected = 1;
                          nowPlaying = true;
                          upcoming = false;
                          topRated = false;
                          final movieProvider = Provider.of<MoviesProvider>(
                              context,
                              listen: false);
                          movieProvider.getData('now_playing');
                        });
                      },
                      child: barText("Now Playing", nowPlaying)),
                  SizedBox(
                    width: 24.w,
                  ),
                  InkWell(
                      onTap: () {
                        setState(() {
                          selected = 2;
                          upcoming = true;
                          nowPlaying = false;
                          topRated = false;
                          final movieProvider = Provider.of<MoviesProvider>(
                              context,
                              listen: false);
                          movieProvider.getData('upcoming');
                        });
                      },
                      child: barText("Upcoming", upcoming)),
                  SizedBox(
                    width: 24.w,
                  ),
                  InkWell(
                      onTap: () {
                        setState(() {
                          selected = 3;
                          topRated = true;
                          nowPlaying = false;
                          upcoming = false;
                          final movieProvider = Provider.of<MoviesProvider>(
                              context,
                              listen: false);
                          movieProvider.getData('top_rated');
                        });
                      },
                      child: barText("Top Rated", topRated)),
                ],
              ),
            ),
            Flexible(
              child: Consumer<MoviesProvider>(
                builder: (context, currentState, child) {
                  if (currentState.nowPlayingState == state.initial) {
                    currentState.getData('now_playing');
                  }
                  return currentState.nowPlayingState == state.loaded
                      ? GridView.builder(
                          shrinkWrap: true,
                          itemCount: currentState.dataSucces?.results?.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  childAspectRatio: 2 / 3,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                  mainAxisExtent: 220),
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () =>
                                  Navigator.of(context, rootNavigator: true)
                                      .push(MaterialPageRoute(
                                          builder: (_) => DetailMoviesPage(
                                                dataMovie: currentState
                                                    .dataSucces
                                                    ?.results?[index],
                                              ))),
                              child: Column(
                                children: [
                                  Flexible(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.network(
                                          Urls().rootImageUurl +
                                              (currentState
                                                      .dataSucces
                                                      ?.results?[index]
                                                      .posterPath ??
                                                  "")),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 12.h,
                                  ),
                                  Text(
                                    currentState.dataSucces?.results?[index]
                                            .title ??
                                        "",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.sp),
                                  ),
                                ],
                              ),
                            );
                          })
                      : Shimmer.fromColors(
                          baseColor: Colors.grey[300]!,
                          highlightColor: Colors.grey[100]!,
                          enabled: true,
                          child: GridView.builder(
                              shrinkWrap: true,
                              itemCount: 6,
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 2 / 3,
                                      crossAxisSpacing: 10,
                                      mainAxisSpacing: 10,
                                      mainAxisExtent: 220),
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {},
                                  child: Column(
                                    children: [
                                      Flexible(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: Colors.amber,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 12.h,
                                      ),
                                      Text(
                                        "title movie",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.sp),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        );
                },
              ),
            ),
            /* Flexible(
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                scrollDirection: Axis.vertical,
                children: List.generate(6, (index) {
                  return Column(
                    children: [
                      Flexible(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.network(
                              "https://picsum.photos/id/237/200/300"),
                        ),
                      ),
                      SizedBox(
                        width: 12.h,
                      ),
                      Text(
                        "Stranger Things",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp),
                      ),
                    ],
                  );
                }),
              ),
            ) */
          ],
        ),
      ),
    );
  }

  Column barText(String title, bool active) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              color: active == true ? activeColor : offColor, fontSize: 16.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        active == true
            ? Container(
                color: activeColor,
                width: 19.w,
                height: 2.h,
              )
            : Container()
      ],
    );
  }
}
