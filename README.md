# primaku_test

aplikasi untuk melihat informasi movie dan tv show

## Getting Started

Tutorial install flutter 

A. bahan :
    1. Java Development Kit (JDK); bisa di install melalui android studio.
    2. Android Studio.
    3. Android SDK.
    4. Flutter SDK.
    5. IDE / Teks Editor (disini kita akan menggunakan Visual Studio Code)

B. Langkah-langkah :
    1. donwload flutter di - https://docs.flutter.dev/get-started/install
    2. setelah di download lalu extract file lalu simpan di tempat yang diinginkan
    2. download android studio di - https://developer.android.com/studio
    3. setelah di download lalu lakukan installasi dan download semua yang diperlukan ketika installasi seperti SDK dan lain-lain (ikuti saja step installasinya)
    4. download vscode di - https://code.visualstudio.com/
    5. setelah download lalu lakukan installasi, setelah selesai installasi masuk ke menu extensions lalu cari extension "flutter" lalu install dan "dart" juga.
    6. buat aplikasi pertama, buka vscode lalu menu -> command palette atau bisa tekan Ctrl+Shift+P pada keyboard
    7. lalu ketik "flutter" dan pilih Flutter: New Project
    8. masukkan nama project dan tekan enter untuk memilih tempat penyimpanan project
    9. lalu tekan F5 untuk menjalankan flutter di emulator, jika emulator belum di setup maka harus buka android studio lalu pada tombol configure -> klik SDK Manager. lalu pilih versi android yang mau didownload , setelah selesai kembali ke tombol configure -> klik AVD Manager, lalu “Create Virtual Device”, setelah terbuka pilih device yang ingin di gunakan lalu next hingga selesai. Kembali ke vsCode dan jalankan project seperti diatas.

Tutorial build apk 

A. debug app
    1. masuk ke project flutter dengan menggunakan vsCode lalu buka terminal
    2. jalankan perintah "flutter build apk --debug"
    3. selesai dan ambil file apk pada folder "debug/app/outputs/flutter-apk/apk-debug"
B. release app
    1. masuk ke project flutter dengan menggunakan vsCode
    2. tambahkan icon pada aplikasi dengan mengganti icon awal pada folder "/android/app/src/main/res/"
    3. tambahkan key aplikasi dengan perintah "keytool -genkey -v -keystore ~/upload-keystore.jks -keyalg RSA -keysize 2048 -validity 10000 -alias upload" pada terminal 
    4. setelah disimpan lalu buat file key.properties pada folder /android
    5. ubah build.gradle pada folder /android/app 

        buildTypes {
       release {
           // TODO: Add your own signing config for the release build.
           // Signing with the debug keys for now,
           // so `flutter run --release` works.
           signingConfig signingConfigs.debug
       }
         }

        MENJADI, 

         signingConfigs {
       release {
           keyAlias keystoreProperties['keyAlias']
           keyPassword keystoreProperties['keyPassword']
           storeFile keystoreProperties['storeFile'] ? file(keystoreProperties['storeFile']) : null
           storePassword keystoreProperties['storePassword']
            }
        }
        buildTypes {
            release {
                signingConfig signingConfigs.release
            }
        }
    
    6. pada build.gradle yang sama tambahkan informasi keystore sebelum android block

         def keystoreProperties = new Properties()
        def keystorePropertiesFile = rootProject.file('key.properties')
        if (keystorePropertiesFile.exists()) {
            keystoreProperties.load(new FileInputStream(keystorePropertiesFile))
        }

        android {
                ...
        }

    7. masuk ke terminal lalu jalankan perintah "flutter build apk" atau "flutter build apk --split-per-abi" untuk ukuran apk yang di lebih kecil
    9. jika ingin upload ke playstore maka jalankan perintah "flutter build appbundle"
    8. selesai
    


