import 'package:flutter/material.dart';

// widget untuk dissmiss ketika diklik dimana pun selain widget penting
class DismissWidget extends StatefulWidget {
  final Widget? child;
  const DismissWidget({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  _DismissWidgetState createState() => _DismissWidgetState();
}

class _DismissWidgetState extends State<DismissWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        //untuk memasukkan keyboard yang aktif
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus &&
            currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus?.unfocus();
        }
      },
      child: widget.child,
    );
  }
}
