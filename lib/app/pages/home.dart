import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:primaku_test/data/provider/popular_movie.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

import 'detail_movies_page.dart';

class HomeMoviePage extends StatefulWidget {
  const HomeMoviePage({Key? key}) : super(key: key);

  @override
  State<HomeMoviePage> createState() => _HomeMoviePageState();
}

class _HomeMoviePageState extends State<HomeMoviePage> {
  Color activeColor = const Color(0xffFF8F71);
  Color offColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Const().primaryColor,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15.h,
            ),
            Row(
              children: [
                Text(
                  "Stream ",
                  style: TextStyle(
                      color: const Color(0xffFF8F71), fontSize: 24.sp),
                ),
                Text(
                  "Everywhere",
                  style: TextStyle(color: Colors.white, fontSize: 24.sp),
                ),
              ],
            ),
            SizedBox(
              height: 28.h,
            ),
            Image.asset(
              "assets/stream.png",
            ),
            SizedBox(
              height: 24.h,
            ),
            SizedBox(
              height: 55.h,
              child: Text(
                "Popular",
                style: TextStyle(color: Colors.white, fontSize: 16.sp),
              ),
            ),
            const ComplicatedImage()
            /* Flexible(
              child: GridView.count(
                primary: false,
                padding: const EdgeInsets.all(20),
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                crossAxisCount: 2,
                scrollDirection: Axis.vertical,
                children: List.generate(6, (index) {
                  return Column(
                    children: [
                      Flexible(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.network(
                              "https://picsum.photos/id/237/200/300"),
                        ),
                      ),
                      SizedBox(
                        width: 12.h,
                      ),
                      Text(
                        "Stranger Things",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp),
                      ),
                    ],
                  );
                }),
              ),
            ) */
          ],
        ),
      ),
    );
  }

  // bar text
  Column barText(String title, bool active) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              color: active == true ? activeColor : offColor, fontSize: 16.sp),
        ),
        SizedBox(
          height: 8.h,
        ),
        active == true
            ? Container(
                color: activeColor,
                width: 19.w,
                height: 2.h,
              )
            : Container()
      ],
    );
  }
}

// slider
class ComplicatedImage extends StatelessWidget {
  const ComplicatedImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final popularMovie = Provider.of<PopularMoviesProvider>(context);
    if (popularMovie.popularState == state.initial) {
      popularMovie.getData();
    }
    return popularMovie.popularState == state.loaded
        ? CarouselSlider(
            options: CarouselOptions(
              autoPlay: true,
              aspectRatio: 2.0,
              height: 290.h,
              enlargeCenterPage: true,
            ),
            items: popularMovie.dataSucces?.results?.map((data) {
              return Builder(
                builder: (BuildContext context) {
                  return InkWell(
                    onTap: () => Navigator.of(context, rootNavigator: true)
                        .push(MaterialPageRoute(
                            builder: (_) => DetailMoviesPage(
                                  dataMovie: data,
                                ))),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.symmetric(horizontal: 5.0),
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.network(
                          Urls().rootImageUurl + (data.posterPath ?? ""),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  );
                },
              );
            }).toList(),
          )
        : Shimmer.fromColors(
            baseColor: Colors.grey[300]!,
            highlightColor: Colors.grey[100]!,
            enabled: true,
            child: CarouselSlider(
              options: CarouselOptions(
                autoPlay: true,
                aspectRatio: 2.0,
                height: 290.h,
                enlargeCenterPage: true,
              ),
              items: [1, 2, 3, 4, 5].map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: const EdgeInsets.symmetric(horizontal: 5.0),
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.network(
                          "https://picsum.photos/id/237/200/300",
                          fit: BoxFit.cover,
                        ),
                      ),
                    );
                  },
                );
              }).toList(),
            ),
          );
  }
}
