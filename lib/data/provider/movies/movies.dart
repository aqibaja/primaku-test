import 'package:flutter/widgets.dart';
import 'package:primaku_test/data/models/model.dart';
import 'package:primaku_test/data/repositories/constant/constant.dart';
import 'package:primaku_test/data/repositories/services/movies_services.dart';
import 'package:flutter_network_connectivity/flutter_network_connectivity.dart';

// enum PopularState { initial, loading, loaded, error, noInternet }

class MoviesProvider extends ChangeNotifier {
  state _nowPlayingState = state.initial;
  GeneralMovieModel? _dataSucces;

  state get nowPlayingState => _nowPlayingState;
  GeneralMovieModel? get dataSucces => _dataSucces;

  Future getData(String typeGet) async {
    _nowPlayingState = state.loading;
    FlutterNetworkConnectivity flutterNetworkConnectivity =
        FlutterNetworkConnectivity(
      isContinousLookUp:
          true, // optional, false if you cont want continous lookup
      lookUpDuration: const Duration(
          seconds: 5), // optional, to override default lookup duration
      lookUpUrl: 'example.com', // optional, to override default lookup url
    );

    await flutterNetworkConnectivity
        .isInternetConnectionAvailable()
        .then((value) async {
      // if internet Available
      if (value == true) {
        try {
          final data = await MoviesSevices().getNowPlayingMoviesData(typeGet);
          if (data.toString() == "Instance of 'GeneralMovieModel'") {
            _dataSucces = data;
            _nowPlayingState = state.loaded;
          } else {
            _nowPlayingState = state.error;
          }
        } catch (e) {
          _nowPlayingState = state.error;
        }
      } else {
        _nowPlayingState = state.noInternet;
      }
    });
    notifyListeners();
  }
}
